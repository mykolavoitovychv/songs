<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$collection1 = collect([
    'хінкалі', 'хачапурі', 'сациві', 'чурчхела', 'еларджі',
    'кубдарі', 'мцваді', 'чвіштарі', 'чахохбілі', 'ачма', 'мацоні',
    'чахохбілі',
]);

$collection2 = collect([
    '', '', '', 'по аджарськи', 'по мегрельськи', 'по імертінски',
    'з бараниною', 'на грілі', 'з м\'ясом', 'з зеленню',
]);

$collection3 = collect([
    'борошно',
    'сіль',
    'масло вершкове',
    'яйце куряче',
    'сир сулугуні',
    'бринза',
    'сметана',
    'баранина',
    'зелень',
    'перець',
    'фета',
    'маслини',
    'оливки',
    'мікс салату',
]);

$factory->define(Product::class, function (Faker $faker) use ($collection1, $collection2, $collection3) {
    return [
        /*'title' => mb_convert_case(
            trim($collection1->random() . ' ' . $collection2->random()),
            MB_CASE_TITLE_SIMPLE,
            'UTF-8'
        ),*/
        'description' => $collection3->random(rand(4, 8))->implode(', '),
        'price' => round(rand(100, 200) / rand(1, 3), 2),
        'weight' => rand(30, 300) . ' г',
        'category_id' => null,
        'is_active' => 1,
    ];
});

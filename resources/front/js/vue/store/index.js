import Vue from 'vue'
import Vuex from 'vuex'

// import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    // modules: {},
    state: {
        songs: null
    },
    getters: {
        getSongs: (state) => state.songs
    },
    mutations: {
        setSongs (state, songs) {
            state.songs = songs
        }
    },
    strict: process.env.NODE_ENV !== 'production'
})

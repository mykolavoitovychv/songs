import Vue from 'vue'
import store from './store'

import Songs from './components/Songs'

// let payloadData = document.querySelector('#payload')
// if (payloadData) {
//     let payload = payloadData.dataset.payload ? JSON.parse(payloadData.dataset.payload) : {}
//
//     if (payload.songs && !store.state.songs) {
//         store.commit('songs', payload.songs)
//     }
// }

if (document.getElementsByClassName('#songs')) {
    new Vue({
        el: '#songs',
        store,
        components: {
            Songs
        }
    })
}

export function setOnLoadListeners () {
    let cartItems = document.querySelectorAll('.c-cart-item')

    if (cartItems) {
        cartItems.forEach(function (cartItem) {
            let cartItemImg = cartItem.querySelector('.js-cart-lazy')
            if (cartItemImg) {
                cartItemImg.onload = function () {
                    cartItem.classList.remove('has-loader')
                }
                cartItemImg.onerror = function () {
                    cartItem.classList.remove('has-loader')
                }
            }
        })
    }
}

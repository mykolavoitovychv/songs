import axios from 'axios'
import debounce from 'lodash/debounce'
import {setOnLoadListeners} from './load-listener'

let cartModal = document.querySelector('#cart-products');

if (cartModal) {
    document.querySelector('#cart-products').addEventListener('DOMSubtreeModified', event => {
        setListeners(document.querySelector('#cart-products'))
    })
}

setOnLoadListeners()

function setListeners(cartModal) {
    let count = cartModal.querySelector('.delivery-cart-amount');

    if (count) {
        let btnsCountAdd = cartModal.querySelectorAll('.btn-counter.btn-count--add')
        if (btnsCountAdd) {
            btnsCountAdd.forEach(function (btnCountAdd) {
                btnCountAdd.onclick = event => {
                    event.preventDefault()

                    let product = event.target.closest('.c-cart-item')
                    let oldAmount = Number(product.querySelector('.js-input-count').innerHTML)
                    let amount = oldAmount + 1
                    product.querySelector('.js-input-count').innerHTML = amount
                    updateAmount(oldAmount, amount, product)
                }
            })
        }

        let btnsCountRemove = cartModal.querySelectorAll('.btn-counter.btn-count--remove')
        if (btnsCountRemove) {
            btnsCountRemove.forEach(function (btnCountRemove) {
                btnCountRemove.onclick = event => {
                     event.preventDefault()

                    let product = event.target.closest('.c-cart-item')
                    let oldAmount = Number(product.querySelector('.js-input-count').innerHTML)
                    let amount = 1

                    if (oldAmount && oldAmount > 1) {
                        amount = oldAmount - 1
                    }

                    product.querySelector('.js-input-count').innerHTML = amount
                    updateAmount(oldAmount, amount, product)
                }
            })
        }

        let btnsRemove = cartModal.querySelectorAll('.item-remove')
        if (btnsRemove) {
            btnsRemove.forEach(function (btnRemove) {
                btnRemove.onclick = function (event) {
                    event.preventDefault()

                    let product = event.target.closest('.c-cart-item')
                    product.style.opacity = '0.5'
                    let oldAmount = Number(product.querySelector('.js-input-count').innerHTML)
                    updateAmount(oldAmount, 0, product)
                }
            })
        }
    }
}

var updateAmount = debounce(function (oldAmount, amount, product, btnsCountRemove) {
    let cartItems = document.querySelectorAll('.c-cart-item')
    if (cartItems) {
        cartItems.forEach(function (cartItem) {
            cartItem.classList.add('has-loader')
        })
    }
    const url = route('cart.set_amount')

    return axios.post(url, {
        id: product.dataset.id,
        amount
    }).then((response) => {
        window.renewCart(response.data.data)
        setOnLoadListeners ()
    }).catch((errors) => {
        setOnLoadListeners ()
        product.querySelector('.js-input-count').innerHTML = oldAmount

        if (amount == 0) {
            product.style.opacity = ''
        }

        if (errors.response && errors.response.data && errors.response.data.errors) {
            console.error({...errors.response.data.errors})
        }
    })
}, 500)

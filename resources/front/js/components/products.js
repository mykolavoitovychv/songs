let products = document.querySelectorAll('.product-item-js');

if (products) {
    products.forEach(function (product) {
        let data = JSON.parse(product.dataset.product)

        let elements = product.querySelectorAll('.open-buy-modal');

        if (elements) {
            elements.forEach(function (element) {
                element.addEventListener('click', event => {
                    event.preventDefault()

                    let buyModal = document.getElementById('popup-dish');
                    buyModal.dataset.id = data.id
                    buyModal.querySelector('#popup-dish-title').innerHTML = data.title
                    buyModal.querySelector('#popup-dish-weight').innerHTML = data.weight
                    buyModal.querySelector('#popup-dish-description').innerHTML = data.description
                    buyModal.querySelector('#popup-dish-price').dataset.price = data.price
                    buyModal.querySelector('#popup-dish-price').innerHTML = data.price + ' ₴'
                    buyModal.querySelector('.js-input-count').innerHTML = 1
                    buyModal.querySelector('.btn-counter.btn-count--remove').setAttribute("disabled", true)

                    new Modal(buyModal).show()
                    var img = new Image();
                    img.src = data.modalImage['1060x760'];
                    img.onload = function() {
                        buyModal.querySelector('#popup-dish-img').src = data.modalImage['1060x760']
                    };
                })
            })
        }
    })
}

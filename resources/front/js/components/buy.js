import axios from 'axios'
import {setOnLoadListeners} from './load-listener'

let buyModal = document.querySelector('#popup-dish');

if (buyModal) {
    buyModal.addEventListener('hidden.bs.modal', function () {
        let img = buyModal.querySelector('#popup-dish-img')
        img.src = img.dataset.defaultImg
    });

    let count = buyModal.querySelector('.js-input-count');

    if (count) {
        let removeButton = buyModal.querySelector('.btn-counter.btn-count--remove')
        buyModal.querySelector('.btn-counter.btn-count--add').addEventListener('click', event => {
            event.preventDefault()

            let price = buyModal.querySelector('#popup-dish-price').dataset.price
            let amount = Number(count.innerHTML)
            amount++

            if (amount > 1 && removeButton.disabled) {
                removeButton.removeAttribute("disabled", false);
            }
            let value = amount * Number(price)
            buyModal.querySelector('.js-input-count').innerHTML = amount
            buyModal.querySelector('#popup-dish-price').innerHTML = value + ' ₴'
        })

        removeButton.addEventListener('click', event => {
             event.preventDefault()

            let price = buyModal.querySelector('#popup-dish-price').dataset.price
            let amount = Number(count.innerHTML)
            if (amount) {
                if (amount > 1) {
                    amount --
                }
                if (amount == 1) {
                    removeButton.setAttribute("disabled", true);
                }
            }

            let value = amount * Number(price)
            buyModal.querySelector('.js-input-count').innerHTML = amount
            buyModal.querySelector('#popup-dish-price').innerHTML = value + ' ₴'
        })

        buyModal.querySelector('.btn-buy').addEventListener('click', function (event) {
            event.preventDefault()

            let button = event.target
            if (event.target.type !== 'submit') {
                button = event.target.closest('button.btn-buy')
            }
            button.disabled = true
            const url = route('cart.append-product')
            axios.post(url, {
                id: buyModal.dataset.id,
                amount: Number(count.innerHTML)
            }).then((response) => {
                window.renewCart(response.data.data)
                new Modal(buyModal).hide()
                button.disabled = false
                setOnLoadListeners()
            }).catch((err) => {
                console.error(err)
                button.disabled = false
            })
        })
    }
}

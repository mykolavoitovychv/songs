import './products'
import './buy'
import './cart'
import './modal-thanks'
import { initLoader } from './initLoader'
import LazyLoad from 'vanilla-lazyload'

import { load } from 'recaptcha-v3'

window.renewCart = (cart) => {
    let amount = 0
    let price = 0
    let products = cart.products

    if (products) {
        products.forEach(function (product) {
            amount += product.amount
        })
        price = cart.total_price
    }

    document.querySelector('#cart-products-list').innerHTML = cart.products_html

    let cartLoad = new LazyLoad({
        elements_selector: '.js-cart-lazy'
    });

    document.querySelectorAll('.delivery-cart-amount').forEach(function (element) {
        element.innerHTML = amount + ' ' + customPlural('страва', 'страви', 'страв', amount)
    })

    document.querySelectorAll('.delivery-cart-price').forEach(function (element) {
        element.innerHTML = price
    })

    document.querySelectorAll('.delivery-price').forEach(function (element) {
        if (cart.delivery_price) {
            element.innerHTML = cart.delivery_price + ' ₴'
        } else {
            element.innerHTML = 'безкоштовно'
        }

    })

    if (amount > 0) {
        document.getElementById('cart-products').classList.remove('is-empty')
        document.querySelectorAll('.show-if-products').forEach(function (element) {
            element.style.display = ''
        })
        document.querySelectorAll('.hide-if-products').forEach(function (element) {
            element.style.display = 'none'
        })
        document.getElementById('cart-submit-button').classList.remove("disabled")
    } else {
        document.getElementById('cart-products').classList.add('is-empty')
        document.querySelectorAll('.show-if-products').forEach(function (element) {
            element.style.display = 'none'
        })
        document.querySelectorAll('.hide-if-products').forEach(function (element) {
            element.style.display = ''
        })
        document.getElementById('cart-submit-button').classList.add("disabled")
    }
    initLoader();
}

window.sendForm = (url, form, thanksId = null, redirect = false) => {
    let inputs = form.querySelectorAll('input.is-invalid, textarea.is-invalid')
    Array.from(inputs, (input) => {
        input.classList.remove('is-invalid')
    })

    let errors = form.querySelectorAll('.invalid-feedback .error')
    Array.from(errors, (error) => {
        if (error) {
            error.innerHTML = ''
        }
    })

    let data = new FormData(form)
    disabledElements(form, true)

    load(window.captchaSiteKey, {
        useRecaptchaNet: true,
        autoHideBadge: true
    }).then((recaptcha) => {
        recaptcha.execute('submit').then((token) => {
            data.append('g-recaptcha-response', token)

            axios.post(url, data)
                .then(response => {
                    if (redirect) {
                        let payment_link = response.data.payment.payment_link
                        if (payment_link) {
                            window.location.replace(payment_link)
                        }
                    } else {
                        if (thanksId) {
                            let thanksModal = new bsn.Modal(document.getElementById(thanksId))
                            thanksModal.show()

                            setTimeout(function () {
                                thanksModal.hide()
                            }, 7000)
                        }

                        form.reset()
                        disabledElements(form, false)
                    }
                })
                .catch(error => {
                    disabledElements(form, false)
                    console.error(error)
                    let errors = error.response.data.errors
                    for (var errorKey in errors) {
                        form.querySelector(`input[name="${errorKey}"], textarea[name="${errorKey}"]`).classList.add('is-invalid')
                        form.querySelector('.invalid-feedback .error.' + errorKey).innerHTML = errors[errorKey]
                    }
                })
        })
    })
}

function customPlural (textSingle, textMultiple, textMany, count) {
    if (count === 1) {
        return textSingle
    } else if (count > 1 && count < 5) {
        return textMultiple
    }

    return textMany
}

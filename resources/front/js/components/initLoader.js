import {TweenMax, SlowMo} from "gsap/all";

export function initLoader() {
    let svgLoader = document.querySelectorAll('#paint0_linear');
    if(svgLoader) {
        svgLoader.forEach((item) => {
            TweenMax.fromTo(item, 2.5,
                {
                    attr:{ x1: '-100%', x2: '0%', y1: '-100%', y2: '0%' },
                    rotate: 45
                },
                {
                    attr:{ x1: '100%', x2: '200%', y1: '100%', y2: '200%' },
                    rotate: 45,
                    repeat: -1,
                    ease: SlowMo.ease.config(0.25, 0.5, false)
                }
            );
        });
    }
};

let btnClose = document.getElementById('closeThanks')

if (btnClose) {
    btnClose.addEventListener('click', () => {
        window.location.assign(btnClose.dataset.url)
    })
}


<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, viewport-fit=contain"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta http-equiv="cleartype" content="on"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="address=no"/>

    <title>
        Guess Song
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content=""/>
    <meta name="copyright" content="(c)">
    <meta http-equiv="Reply-to" content="noreply@suliguli.com">

    <meta name="theme-color" content="#000000">


    <!-- Twitter card -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@twitter" />
    <meta name="twitter:creator" content="@twitter" />
    <meta name="twitter:image" content="https://example.com/example.png" />


    <!-- Open Graph -->
<meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="https://example.com/example1200x630.png" />
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:site_name" content="Suli Guli" />
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>


    <!-- Styles -->
    @section('head-styles')
        <link href="{{ static_asset('pub/css/app.css', 'build/pub', 'build') }}" rel="stylesheet" type="text/css">
    @show

    <script>
        window.App = {
            baseUrl: '/',
            staticUrl: '/',
            cdn: '../src/js/'
        }

        window.staticRoot = '{{ rtrim(config('app.static_url'), '/') . '/front/' }}';

        window.locale = '{{ App::getLocale() }}';

        window.staticAsset = function (path) {
            return '{{ asset('front/:path:') }}'.replace(':path:', path);
        };

        window.recaptcha_public_key = '{{ env('RECAPTCHA_PUBLIC_KEY') }}';
    </script>
</head>

<footer class="c-footer">
    <div class="o-box">

        <div class="c-footer-inner">
            <div class="c-footer-col">
                <h2 class="c-footer-title toogle js-footer-title wow fadeInUp">Ресторани</h2>
                <div class="c-social">
                    <ul class="c-social-list wow fadeInUp" data-wow-delay=".1s">
                        <li class="c-social-item">
                            <a href="https://instagram.com/suliguli.pekarnia" class="c-social-link">
                                <img src="{{ static_asset('img/svg/instagram.svg', 'build/layouts', 'build') }}" alt="instagram">
                            </a>
                        </li>
                        <li class="c-social-item">
                            <a href="https://www.facebook.com/suliguli.pekarnya/" class="c-social-link">
                                <img src="{{ static_asset('img/svg/facebook.svg', 'build/layouts', 'build') }}" alt="facebook">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="c-footer-col__toogle">

                    <div class="c-footer-address wow fadeInUp" data-wow-delay=".2s">
                        <h3 class="c-footer-address__title">Вараш</h3>
                        <address><span>ТРЦ Оранж Плаза</span> <br> Майдан Незалежності, 10</address>
                        <a class="number" href="tel: +380 68 917 22 96">+380 68 917 22 96</a><br>
                        <a class="number" href="tel: +380 95 323 77 91">+380 95 323 77 91</a><br>
                        <time>ПН–НД, 9–22</time>
                    </div>
                </div>
            </div>
            <div class="c-footer-col ">
                <h2 class="c-footer-title toogle js-footer-title wow fadeInUp">Пекарні</h2>

                <div class="c-social">
                    <ul class="c-social-list wow fadeInUp" data-wow-delay=".1s">
                        <li class="c-social-item">
                            <a href="https://instagram.com/suliguli.pekarnia" class="c-social-link" target="_blank" rel="noopener">
                                <img src="{{ static_asset('img/svg/instagram.svg', 'build/layouts', 'build') }}" alt="instagram">
                            </a>
                        </li>
                        <li class="c-social-item">
                            <a href="https://www.facebook.com/suliguli.pekarnya/" class="c-social-link" target="_blank" rel="noopener">
                                <img src="{{ static_asset('img/svg/facebook.svg', 'build/layouts', 'build') }}" alt="facebook">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="c-footer-col__toogle">

                    <div class="c-footer-address wow fadeInUp" data-wow-delay=".2s">
                        <h3 class="c-footer-address__title">Луцьк</h3>
                        <address>вул. Набережна, ‌‌‎2</address>
                        <a class="number" href="tel: +38‌‌‎0 95 278 70 77">+38‌‌‎0 95 278 70 77</a><br>
                        <time>ПН–НД, 8–21</time>
                    </div>

                    <div class="c-footer-address wow fadeInUp" data-wow-delay=".2s">
                        <h3 class="c-footer-address__title">Луцьк</h3>
                        <address>вул. Задворецька, 1</address>
                        <a class="number" href="tel: +380 98 797 70 77">+380 98 797 70 77</a><br>
                        <time>ПН–НД, 7–20</time>
                    </div>

                    <div class="c-footer-address wow fadeInUp" data-wow-delay=".2s">
                        <h3 class="c-footer-address__title">Нетішин</h3>
                        <address>вул. Шевченка, 7</address>
                        <a class="number" href="tel: +380 97 378 07 74">+380 97 378 07 74</a><br>
                        <time>ПН–НД, 9–21</time>
                    </div>
                </div>
            </div>
        </div>

        <div class="c-footer-bottom">
            <div class="c-footer-copyright wow fadeInUp">&copy;&nbsp;{{ date('Y') }} Сулі Гулі</div>
            <div class="c-ideil wow fadeInUp">
                <a class="c-ideil-link" href="https://www.ideil.com/">
                    Зроблено в
                    <span class="sr-only">ideil.</span>
                    <img class="c-ideil-img" src="{{ static_asset('img/svg/ideil.svg', 'build/layouts', 'build') }}"
                         alt="ideil. — розробка веб-сайтів, інтернет-магазинів, мобільних додатків та програмного забезпечення"
                         aria-hidden="true">
                </a>
            </div>
        </div>
    </div>
</footer>

<div
    class="c-home-cart js-cart-popup
    @if(!$products = \App\Cart::currentProducts())
        is-empty
    @endif"
    id="cart-products"
>
    <section class="c-home-cart__header show-if-products"
        @if(!$products)
            style="display: none"
        @endif
    >
        <span class="title">Моє замовлення</span>
        <span class="c-cart-subtitle">
            В кошику <b class="delivery-cart-amount">{{ \App\Cart::currentProductsAmount() }} {{ trans_choice('front.страва', \App\Cart::currentProductsAmount()) }}</b>
        </span>
        <button class="btn btn-close js-home-cart">
            <img src="{{ static_asset('/img/svg/close.svg', 'build/layouts', 'build') }}" alt="close">
        </button>
    </section>
    <section class="c-home-cart__content js-scroll-y show-if-products">
        <div class="c-cart ">
            <ul class="c-cart-list" id="cart-products-list">
                @if($products)
                    @each('front::components.modals.cart_item', $products, 'product')
                @endif
            </ul>
        </div>
    </section>
    <section class="c-home-cart__footer show-if-products">
        <div class="delivery">
            <span>Доставка:</span>
            @if($deliveryPrice = \App\Cart::current()->getDeliveryPrice())
                <b class="price delivery-price">{{ round($deliveryPrice) }} ₴</b>
            @else
                <b class="price delivery-price">безкоштовно</b>
            @endif
        </div>
        <div class="total">
            <span>Всього до оплати:</span>
            <b class="price"><b class="delivery-cart-price">{{ round(\App\Cart::current()->getTotalPrice()) }}</b> ₴</b>
        </div>
        <a
            href="{{ route('order.index') }}"
            class="btn @if(!$products) disabled @endif btn-primary"
            id="cart-submit-button"
        >Оформити замовлення</a>
    </section>
    <section class="o-notification hide-if-products"
         @if($products)
             style="display: none"
         @endif
    >
        <span class="o-notification-title">Йой. <br> Тут порожньо:(</span>
        <div class="o-notification-img">
            <div class="animation-sad">
                <img src="{{ static_asset('/img/svg/khachapur-sad.svg', 'build/layouts', 'build') }}" alt="">
            </div>
        </div>

        <p>Вибери страву з&nbsp;нашого меню, а&nbsp;ми&nbsp;доставимо її&nbsp;якомога швидше!</p>

        <button class="btn btn-primary js-home-cart">Назад в меню</button>
    </section>
</div>

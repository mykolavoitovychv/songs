<div class="modal fade" id="popup-dish" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered c-dish-wrap" role="document">
            <div class="modal-content c-dish-content">
                <div class="c-dish">
                    <div class="c-dish-close">
                        <button class="btn btn-close" data-dismiss="modal" aria-label="Close">
                            <img src="{{ static_asset('/img/svg/close.svg', 'build/layouts', 'build') }}" alt="close">
                        </button>
                    </div>

                    <aside class="c-dish-img">
                        <picture>
                            <img id="popup-dish-img" data-toggle="modal" data-target="#popup-dish" alt="" class="lazy"
                                src="{{ asset('front/img/default.jpg') }}" data-default-img="{{ asset('front/img/default.jpg') }}"
                            />
                        </picture>
                    </aside>
                    <div class="c-dish-info js-scroll-dish">
                        <section class="c-dish-head">
                            <span class="c-product-title">
                                <b id="popup-dish-title"></b>
                            </span>
                            <span class="weight" id="popup-dish-weight"></span>
                            <p class="c-product-ingredients" id="popup-dish-description"></p>
                            <span class="price"><b id="popup-dish-price"></b></span>
                        </section>
                    </div>
                    <div class="c-dish-cart">
                        <div class="c-counter">
                            <button class="btn btn-counter btn-count--remove" disabled>
                                <img src="{{ static_asset('/img/svg/remove.svg', 'build/layouts', 'build') }}" alt="remove">
                            </button>
                            <span class="c-counter-number js-input-count">1</span>
                            <button class="btn btn-counter btn-count--add">
                                <img src="{{ static_asset('/img/svg/add.svg', 'build/layouts', 'build') }}" alt="add">
                            </button>
                        </div>
                        <button class="btn btn-primary btn-buy" type="submit" >В кошик</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

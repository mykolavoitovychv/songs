<stylesheet src="{{ resource_path('emails/dist/css/app.css') }}"></stylesheet>
<container class="header">
    <wrapper>
        <row class="collapse">
            <columns>
                <a href="{{ url('/') }}" target="_blank" rel="noopener noreferrer">
                    <img src="{{ url('static/emails/img/logo.png') }}" title="Suli Guli" alt="Suli Guli">
                </a>

                <spacer size="24"></spacer>

                <h1>Замовлення прийнято</h1>

                <p>Привіт, {{ $order->name }}!</p>
                <p>
                    Твоє замовлення <a href="{{ url('/') }}" target="_blank" rel="noopener noreferrer"><b>№{{$order->id}}</b></a>
                    створене і&nbsp;
                    @if(\Illuminate\Support\Arr::get($order, 'delivery_time.type') === 'fast')
                        вже найближчим часом
                    @else
                        {{ \Illuminate\Support\Arr::get($order, 'delivery_time.date').' о '.\Illuminate\Support\Arr::get($order, 'delivery_time.time') }}
                    @endif
                    буде тобі доставлене
                </p>

                <spacer size="52"></spacer>
            </columns>
        </row>
    </wrapper>
</container>

<container class="body">
    <table class="body-inner">
        <tbody>
        <tr>
            <td>
                <wrapper>
                    <h2>Склад замовлення</h2>

                    @foreach($order->items as $orderItem)
                    @php
                        $product = $orderItem->product
                    @endphp
                        <row class="delivery collapse">
                            @if($product->hasMedia('main'))
                                <columns large="1">
                                    <table class="delivery-image">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img src="{{ url($product->getFirstMediaUrl('main', '320x320')) }}" width="80" height="80" alt="{{ $product->title }}">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <spacer size="16" class="hide-for-large-custom"></spacer>
                                </columns>
                            @endif

                            <columns>
                                <p><b>{{ $product->title }}</b> {{ $product->weight }}</p>
                                    <p><b>
                                        {{ round($orderItem->product_price) }} UAH
                                    </b></p>
                            </columns>

                            <columns large="1">
                                <p class="text-right"><b>{{ $orderItem->amount }} шт</b></p>
                            </columns>

                            <columns large="1">
                                <p class="text-right"><b>
                                     {{ round($orderItem->base_price) }} UAH
                                </b></p>
                            </columns>
                        </row>

                        <spacer size="16" class="show-for-large"></spacer>
                        <spacer size="32" class="hide-for-large-custom"></spacer>
                    @endforeach

                    <hr />

                    <row class="collapse">
                        <columns>
                            <p><small><b>Доставка</b></small></p>
                        </columns>

                        <columns>
                            <p class="text-right"><small><b>{{ round($order->delivery_price) }} UAH</b></small></p>
                        </columns>
                    </row>
                    <row class="collapse">
                        <columns>
                            <p><small><b>{{ $order->name }},</b></small></p>
                            <p><a href="tel:{{ $order->phone }}"><small><b>{{ $order->phone }}</b></small></a></p>
                            <spacer size="14"></spacer>
                            <p>
                                <small>
                                    <b>
                                        {{ \Illuminate\Support\Arr::get($order, 'address.street') }}
                                        {{  \Illuminate\Support\Arr::get($order, 'address.house') }},
                                        @if ($entrance = \Illuminate\Support\Arr::get($order->address, 'entrance'))
                                            {{ $entrance }} під'їзд,
                                        @endif
                                        @if ($code = \Illuminate\Support\Arr::get($order->address, 'code'))
                                            код домофону - {{ $code }},
                                        @endif
                                        @if ($floor = \Illuminate\Support\Arr::get($order->address, 'floor'))
                                            {{ $floor }} поверх,
                                        @endif
                                        @if ($flat = \Illuminate\Support\Arr::get($order->address, 'flat'))
                                            квартира {{ $flat }}
                                        @endif
                                    </b>
                                </small>
                            </p>

                            <spacer size="32" class="hide-for-large-custom"></spacer>
                        </columns>

                        <columns>
                            <p class="text-right"><small>Всього</small></p>
                            <h3 class="text-right">{{ round($order->total_price) }} UAH</h3>
                        </columns>
                    </row>
                </wrapper>

                <table class="overlay">
                    <tbody>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</container>
<container class="footer">
    <wrapper>
        <row class="collapse">
            <columns>
                <h2>Дякуємо, що&nbsp;ти&nbsp;з&nbsp;нами</h2>

                <p>З&nbsp;приводу питань роботи доставки та&nbsp;здійснення замовлення</p>
                <h3>📞 <a href="tel:{{ optional($settings->firstWhere('title', 'phone'))->value }}"><b>{{ optional($settings->firstWhere('title', 'phone'))->value }}</b></a></h3>
            </columns>
        </row>
    </wrapper>
</container>

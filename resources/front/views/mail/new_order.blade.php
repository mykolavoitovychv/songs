@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot
    <div style="width: 700px">
        <h1>Замовлення №{{ $order->id }}</h1>
        <p>Ім'я: {{ $order->name }}</p>
        <p>Телефон: {{ $order->phone }}</p>
        <p>Створено: {{ $order->created_at->format('d.m.Y h:i:s') }}</p>
        <p>
            <b>
                Час доставки: {{ $order->getDeliveryTime() }}
            </b>
        </p>
        <p>Адреса:
            <b>{{ \Illuminate\Support\Arr::get($order->address, 'street') }}, {{ \Illuminate\Support\Arr::get($order->address, 'house') }},</b>
            @if ($entrance = \Illuminate\Support\Arr::get($order->address, 'entrance'))
                {{ $entrance }} під'їзд,
            @endif
            @if ($code = \Illuminate\Support\Arr::get($order->address, 'code'))
                код домофону - {{ $code }},
            @endif
            @if ($floor = \Illuminate\Support\Arr::get($order->address, 'floor'))
                {{ $floor }} поверх,
            @endif
            @if ($flat = \Illuminate\Support\Arr::get($order->address, 'flat'))
                квартира {{ $flat }}
            @endif
        </p>
        <p>Доставка: <b>{{  $order->delivery_price }}&nbsp;грн.</b></p>
        <p>Сума покупки: <b>{{ $order->products_price }} грн.</b></p>
        <p>Ціна: <b>{{ $order->total_price }} грн.</b></p>
        <p>Спосіб оплати: <b>{{ $order->payType }}</b></p>
        <p>Коментар: {{ $order->comment }}</p>
        <table style="width: 700px">
            <thead>
                <tr style="border-bottom: 1px solid black;">
                    <td>Назва</td>
                    <td style="text-align: center;">Кількість</td>
                    <td style="text-align: center;">Ціна</td>
                    <td style="text-align: center;">Вартість</td>
                </tr>
            </thead>
            <tbody>
                @foreach($order->items as $orderItem)
                    @php
                        $product = $orderItem->product
                    @endphp
                    <tr>
                        <td style="padding: 3px; border-right: 1px solid grey">
                            <b>{{ $product->title }}</b> {{ $product->weight }}
                        </td>
                        <td style="padding: 3px; text-align: center; border-right: 1px solid grey">{{ $orderItem->amount }}</td>
                        <td style="padding: 3px; text-align: center; border-right: 1px solid grey">
                            @if($price = $orderItem->product_price)
                                {{ ($price - floor($price) > 0) ? $price : number_format($price, 0, '.', '') }} грн.
                            @endif
                        </td>
                        <td style="padding: 3px; text-align: center;">
                            @if($price = $orderItem->base_price)
                                {{ ($price - floor($price) > 0) ? $price : number_format($price, 0, '.', '') }} грн.
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><hr></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} ideil.
        @endcomponent
    @endslot
@endcomponent

@extends('front::layouts.base')

@section('page-title', 'Оформлення замовлення')


@section('main-class', 'main')

@section('main')
    <div class="c-order"
        id="payload"
        data-payload="{{ json_encode([
            'cart' => new App\Http\Resources\Front\Cart($cart),
            ], JSON_UNESCAPED_UNICODE) }}"
    >
        @include('front::pages.order.components.form', compact('cart', 'delivery'))
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="popup-thanks" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button
                    id="closeThanks"
                    class="btn btn-close"
                    data-dismiss="modal"
                    aria-label="Close"
                    data-url="{{ route('delivery.index') }}"
                >
                    <img src="{{ static_asset('/img/svg/close.svg', 'build/layouts', 'build') }}" alt="close">
                </button>

                <section class="o-notification">
                    <span class="o-notification-title">Дякуємо за&nbsp;замовлення!</span>
                    <div class="o-notification-img">
                        <div class="animation">
                            <img src="{{ static_asset('/img/svg/khachapur.svg', 'build/layouts', 'build') }}" alt="">
                        </div>
                    </div>

                    <p>Найближчим часом вам зателефонує менеджер для підтвердження вашого замовлення.</p>

                    <a href="{{ route('delivery.index') }}" class="btn btn-primary">На головну</a>
                </section>
            </div>
        </div>
    </div>
@endsection

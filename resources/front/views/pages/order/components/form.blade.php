<order-form
    :cart="{{ json_encode(new App\Http\Resources\Front\Cart($cart)) }}"
    id="order-form"
    khachapuri="{{ static_asset('/img/svg/khachapur-sad.svg', 'build/layouts', 'build') }}"
    radio-button-img="{{ static_asset('/img/svg/time.svg', 'build/layouts', 'build') }}"
    delivery-img="{{ static_asset('/img/svg/delivery.svg', 'build/layouts', 'build') }}"
    sub-img="{{ static_asset('/img/svg/remove.svg', 'build/layouts', 'build') }}"
    add-img="{{ static_asset('/img/svg/add.svg', 'build/layouts', 'build') }}"
    working-from="{{ $delivery->working_from }}"
    working-to="{{ $delivery->working_to }}"
/>

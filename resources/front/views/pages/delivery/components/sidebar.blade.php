<aside class="c-home-sidebar js-sidebar">
    <div class="c-category js-category-scroll">

        <ul class="c-category-list js-category">
            @foreach($menuItems as $category)
                <li class="c-category-item">
                    <a href="#" class="c-category-link js-scroll-category
                        @if($loop->first)
                            is-active
                        @endif" data-href="ct-{{ $loop->iteration }}"
                    >
                        <div class="c-category-img">
                            <picture>
                                <source data-srcset="{{ $category->getFirstMediaUrl('main', '36x36') }}, {{ $category->getFirstMediaUrl('main', '72x72') }} 2x" />
                                <img alt="{{ $category->title }}" class="lazy" data-src="{{ $category->getFirstMediaUrl('main', '36x36') }}" />
                            </picture>
                        </div>
                        <span class="d-lg-block d-none">{{ $category->title }}</span>
                        <span class="d-lg-none">{{ $category->title }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</aside>

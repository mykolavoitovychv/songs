<li
    class="c-product-item wow fadeInUp product-item-js"
    data-wow-delay=".2s"
    data-product="{{ json_encode(new App\Http\Resources\Front\Product($product), JSON_UNESCAPED_UNICODE) }}"
>
    <div class="c-product-img open-buy-modal">
        <picture>
            <source data-srcset="{{ $product->getFirstMediaUrl('main', '320x320') }}, {{ $product->getFirstMediaUrl('main', '640x640') }} 2x" />
            <img data-target="#popup-dish" alt="{{ $product->title }}" class="lazy" data-src="{{ $product->getFirstMediaUrl('main', '320x320') }}" />
        </picture>
    </div>
    <h3 class="c-product-title open-buy-modal" data-target="#popup-dish">
        <b>{{ $product->title }}</b>
    </h3>
    <span class="weight">{{ $product->weight }}</span>
    <p class="c-product-ingredients">{{ $product->description }}</p>
    <div class="c-product-order">
        <span class="price"><b>{{ round($product->price) }} ₴</b></span>
        <button class="btn btn-primary open-buy-modal" data-target="#popup-dish">Хочу!</button>
    </div>
</li>

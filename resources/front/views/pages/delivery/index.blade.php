@extends('front::layouts.base')

{{--@if($activeCategory->seo_title)
    @section('page-title', $activeCategory->seo_title)
@endif

@if($activeCategory->seo_description)
    @section('page-description', $activeCategory->seo_description)
@endif--}}

@section('main-class', 'main')

@section('main')
    <div class="c-home">
        <div class="o-box">
            <div class="c-home-inner js-sidebar-scroll">

                @include('front::pages.delivery.components.sidebar', compact('menuItems'))

                <div class="c-home-main">
                    @if($mainHomeBanner && $mainHomeBanner->slides->count())
                        @include('front::pages.delivery.components.banner', compact('mainHomeBanner'))
                    @endif

                    @foreach($menuItems as $item)
                        <section class="c-product-section js-section" data-id="ct-{{ $loop->iteration }}">
                            <h2 class="c-product-subtitle wow fadeInUp" data-wow-delay=".1s">{{ $item->title }}</h2>
                            <ul class="c-product-list">
                                @each('front::pages.delivery.components.product', $item->activeProducts, 'product')
                            </ul>
                        </section>
                    @endforeach

                    <div
                        class="c-home-cart__btns show-if-products"
                        id="delivery-cart"
                        @if(! \App\Cart::currentProducts())
                            style="display:none"
                        @endif
                    >
                        <div class="inner js-home-cart">
                            <section class="total">
                                <span class="count delivery-cart-amount">{{ \App\Cart::currentProductsAmount() }} {{ trans_choice('front.страва', \App\Cart::currentProductsAmount()) }}</span>
                                <b><span class="delivery-cart-price">{{ round(\App\Cart::current()->getTotalPrice()) }}</span> ₴</b>
                            </section>
                            <div class="animation">
                                <img src="{{ static_asset('/img/svg/khachapur.svg', 'build/layouts', 'build') }}" alt="khachapuri">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('front::components.modals.buy')
    @include('front::components.modals.cart')
@endsection


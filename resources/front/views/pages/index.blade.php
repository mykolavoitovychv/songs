@extends('front::layouts.base')

@section('main-class', 'main')

@section('main')
    <div id="app">
        <songs
            id="songs"
            :songs="{{ \App\Song::all([
                'word_1',
                'word_2',
                'word_3',
                'word_4',
                'word_5'
            ]) }}"
        />
    </div>
@endsection


<!doctype html>
<html lang="en">
    @include('front::partials.head')
    <body class="@yield('body-class', '')">
        @yield('main')

        <script src="{{ asset('build/src/vendor/js/lodash.js') }}"></script>
        <script src="{{ asset('build/src/vendor/js/bootstrap-native-v4.js') }}"></script>
        <script src="{{ static_asset('/pub/js/app.js', 'build/pub', 'build') }}"></script>
    </body>
</html>

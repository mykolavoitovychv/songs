<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">
    @include('front::partials.head')
    <body class="@yield('body-class', '')">
{{--        @include('front::partials.header')--}}

        <main class="@yield('main-class', '')">
            @yield('main')
        </main>

{{--        @include('front::partials.footer')--}}

{{--        @yield('modals')--}}

        @routes
        <script src="{{ static_asset('pub/vendor/js/lodash.js', 'build/pub', 'build') }}"></script>

        <script src="{{ static_asset('pub/vendor/js/bootstrap-native-v4.js', 'build/pub', 'build') }}"></script>
        <script src="{{ asset(mix('js/app.js', 'front')) }}"></script>
        <script src="{{ static_asset('/pub/js/app.js', 'build/pub', 'build') }}"></script>

        @yield('aditional-scripts')
    </body>
</html>

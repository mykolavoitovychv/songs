module.exports = {
    'jquery': 'jQuery',
    '$': 'jQuery',
    'vue': 'Vue',
    'vuex': 'Vuex',
    'lodash': '_'
};

import { vMount } from '@helpers';

// import Test from './components/Test.vue';

// vMount('#el', {
//     store,
//     render(h) {
//         return h(Test);
//     }
// });
import LazyLoad from 'vanilla-lazyload'
import { fixScroll } from '../scroll/fix-scroll'
import './btn-shadow'
import './gsap'
const html = document.documentElement;

fixScroll('.modal');

window.myLazyLoad = new LazyLoad({
    elements_selector: '.lazy'
});

new LazyLoad({
    elements_selector: '.js-cart-lazy'
});
const btnOrder = document.querySelectorAll('.js-home-cart');
if(btnOrder) {
    for(let i = 0; i < btnOrder.length; i++) {
        btnOrder[i].addEventListener('click', function() {
            if (html.classList.contains('show-popup-cart')) {
                html.classList.remove('show-popup-cart');
            } else {
                html.classList.add('show-popup-cart');
                fixScroll('.js-cart-popup');
            }
        });
    }
};

// popup delivery cart closing on body click
const closeSearch = function (event) {
    const target = event.target;
    if (html.classList.contains('show-popup-cart') && !target.closest('.js-home-cart') && !target.closest('.js-cart-popup')) {
        html.classList.remove('show-popup-cart');
    }
};

// sidebar catalog toggle
html.addEventListener('click', closeSearch);
html.addEventListener('touchend', closeSearch);

// form
let inputs = document.querySelectorAll('.js-input');
if(inputs) {
    inputs.forEach(item => {
        item.addEventListener('input', function() {
            if(this.value !== '') {
                this.classList.add('is-fill');
            } else {
                this.classList.remove('is-fill');
            }
        })
    });

    window.addEventListener('load', function() {
        inputs.forEach(item => {
            if(item.value !== '') {
                item.classList.add('is-fill');
            } else {
                item.classList.remove('is-fill');
            }
        });
    });
}
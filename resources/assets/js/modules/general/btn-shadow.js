let media = window.matchMedia('(max-width: 991.98px)');
const btnSticky = document.querySelector('.js-btn-shadow');
const cart = document.querySelector('.js-order-sidebar');

function toogleShadow(media) {
    if (media.matches) {
        let top = 0;
        function btnShadow() {
            top = cart.offsetTop + cart.offsetHeight;
            if(btnSticky.offsetTop < top) {
                btnSticky.classList.add('has-shadow')
            } else {
                btnSticky.classList.remove('has-shadow');
            }
        };
        window.addEventListener('load', btnShadow);
        window.addEventListener('scroll', btnShadow);
    }
};

if (btnSticky) {
    toogleShadow(media)
    media.addListener(toogleShadow)
};
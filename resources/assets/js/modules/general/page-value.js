let windowScroll = 0,
    sceenHeight = 0;
function getScrollPage() {
    windowScroll = window.pageYOffset || document.documentElement.scrollTop;
    return windowScroll;
};

window.addEventListener('load', getScrollPage);
window.addEventListener('scroll', getScrollPage);

function getScreenHeight() {
    sceenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
}

window.addEventListener('load', getScreenHeight);
window.addEventListener('resize', getScrollPage);

export { windowScroll, sceenHeight }
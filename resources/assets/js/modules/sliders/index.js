import Swiper from 'swiper';

const banner = document.querySelector('.js-slider-banner');
if(banner) {
    const sliderBanner = new Swiper(banner, {
        spaceBetween: 10,
        navigation: {
            nextEl: '.js-next',
            prevEl: '.js-prev'
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            bulletElement: 'div',
            clickable: true
        }
    });

    let $x = 0;
    sliderBanner.on('sliderMove', function() {
        if(this.touches.currentX > $x) {
            document.querySelectorAll('.swiper-slide').forEach(i => i.classList.remove('to-next'));
            document.querySelectorAll('.swiper-slide').forEach(i => i.classList.add('to-prev'));
        } else if(this.touches.currentX < $x) {
            document.querySelectorAll('.swiper-slide').forEach(i => i.classList.add('to-next'));
            document.querySelectorAll('.swiper-slide').forEach(i => i.classList.remove('to-prev'));
        }
        $x = this.touches.currentX;
    });

    sliderBanner.on('slideChangeTransitionEnd', function() {
        document.querySelectorAll('.swiper-slide').forEach(i => i.classList.remove('to-next', 'to-prev'));
    });
    sliderBanner.on('touchEnd', function() {
        document.querySelectorAll('.swiper-slide').forEach(i => i.classList.remove('to-next', 'to-prev'));
    });
}
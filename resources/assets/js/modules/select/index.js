import NJSelect from 'nj-select';
const selectDate = document.querySelector('.js-select-date');
if(selectDate) {
    new NJSelect(selectDate);
}

const selectTime = document.querySelector('.js-select-time');
if(selectTime) {
    new NJSelect(selectTime);
}
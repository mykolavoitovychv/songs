import _ from 'lodash'
import PerfectScrollbar from 'perfect-scrollbar';
import { windowScroll, sceenHeight } from '../general/page-value'
import animateScroll from './lib-scroll';

let mediaLg = window.matchMedia('(max-width: 991.98px)'),
    media = window.matchMedia('(max-width: 767.98px)'),
    category = document.querySelector('.js-category-scroll'),
    dish = document.querySelector('.js-scroll-dish'),
    initPs = null,
    initDish = null;

function maxH() {
    if(category) {
        if(windowScroll > 113) {
            category.classList.add('mh-sm');
        } else {
            category.classList.remove('mh-sm');
        }
    }
};

window.addEventListener('scroll', maxH);
window.addEventListener('load', maxH);

function initScroll(category) {
    initPs = new PerfectScrollbar(category, {
        suppressScrollX: true,
        swipeEasing: true,
        Wheelpropagation: false
    });
    window.addEventListener('resize', function () {
        if(initPs) {
            initPs.update();
        }
    });
};

function checkMedia(media) {
    if (media.matches) {
        if (initPs) {
            initPs.destroy()
        }
        initPs = null
    } else {
        initScroll(category)
    }
}

if (category) {
    checkMedia(media)
    media.addListener(checkMedia)
};

// dish sroll
function initScrollDish(dish) {
    if(dish) {
        initDish = new PerfectScrollbar(dish, {
            suppressScrollX: true
        });
        window.addEventListener('resize', function () {
            if(initDish) {
                initDish.update();
            }
        });
    }
};

function checkMediaLg (media) {
    if (media.matches) {
        if (initDish) {
            initDish.destroy()
        }
        initDish = null
    } else {
        initScrollDish(dish)
    }
};

if (dish) {
    checkMediaLg(mediaLg)
    media.addListener(checkMediaLg)
};

// scroll to category < 992px
let categoryList = document.querySelector('.js-category');
function scrollToActive(mediaLg) {
    if (mediaLg.matches) {
        let el = categoryList.querySelector('.is-active'),
            left = el.offsetLeft;
        categoryList.scrollLeft = left - 30;
    }
};

function scrollCategory() {
    if (categoryList) {
        scrollToActive(mediaLg)
        mediaLg.addListener(scrollToActive)
    };
};

// scroll to section
let scrollLink = document.querySelectorAll('.js-scroll-category'),
    section = document.querySelectorAll('.js-section');
if(section && scrollLink) {
    function setActive() {
        for(let i = 0; i < section.length; i++) {
            let item = section[i];
            let itemHeight = item.offsetHeight
            if(windowScroll < (item.offsetTop + itemHeight - (sceenHeight / 1.5))) {
                let $id = item.getAttribute('data-id');
                scrollLink.forEach(i => i.classList.remove('is-active'));
                document.querySelector(`.js-scroll-category[data-href="${$id}"]`).classList.add('is-active');
                scrollCategory();
                break;
            };
        };
    };

    window.addEventListener('load', setActive);
    window.addEventListener('resize', setActive);
    window.addEventListener('scroll', setActive);

    
    let timeOut,
        timeHide;
    scrollLink.forEach(item => {
        item.addEventListener('click', _.debounce(function(e) {
            e.preventDefault();
            clearTimeout(timeOut); 
            clearTimeout(timeHide); 
            scrollLink.forEach(i => i.classList.remove('is-active'));
            let $href = this.getAttribute('data-href'),
                $el = document.querySelector(`.js-section[data-id="${$href}"]`),
                $top;
                if(mediaLg.matches) {
                    if(windowScroll > $el.offsetTop) {
                        document.querySelector('.js-sidebar').classList.add('is-hide');
                    } else {
                        document.querySelector('.js-sidebar').classList.remove('is-hide');
                    }
                    timeHide = setTimeout(() => {
                        document.querySelector('.js-sidebar').classList.remove('is-hide');
                    }, 700);
                    if(windowScroll < $el.offsetTop) {
                        $top = $el.getBoundingClientRect().top + window.scrollY - 20;
                    } else {
                        $top = $el.getBoundingClientRect().top + window.scrollY - 120;
                    }
                } else {
                    $top = $el.getBoundingClientRect().top + window.scrollY - 20;
                }
            this.classList.add('is-active');
            window.removeEventListener('scroll', setActive);
            animateScroll($top, 700);
            scrollCategory();
            timeOut = setTimeout(function() {
                window.addEventListener('scroll', setActive);
            }, 1250);
        }, 750, { leading: true }));
    });
}
function fixScroll(el) {
    let arr = document.querySelectorAll(`${el}`);
    Array.from(arr, item => {
        let sidebar = {}
        item.addEventListener('touchstart', function (event) {
            sidebar.allowUp = this.scrollTop > 0
            sidebar.allowDown = this.scrollTop < (this.scrollHeight - this.offsetHeight)
            sidebar.lastY = event.touches[0].pageY
        }, {
            passive: true
        });
        item.addEventListener('touchmove', function (event) {
            let up = event.touches[0].pageY > sidebar.lastY
            let down = !up
    
            if ((up && sidebar.allowUp) || (down && sidebar.allowDown)) {
                event.stopPropagation()
            } else {
                event.preventDefault()
            }
        })
    });
};

export { fixScroll };
import { windowScroll } from '../general/page-value'

let media = window.matchMedia('(max-width: 991.98px)'),
    sidebar = document.querySelector('.js-sidebar'),
    scrollNow, height, top;
if(sidebar) {
    scrollNow = 0;
    height = sidebar.offsetHeight;
    top = sidebar.offsetTop + (sidebar.offsetHeight / 3);
}

function sidebarPosition() {
    if(windowScroll > top) {
        if(scrollNow < windowScroll) {
            sidebar.setAttribute('style', `transform: translateY(-${height}px)`);
        }
        else {
            sidebar.setAttribute('style', `transform: translateY(0px)`);
        }
    }
    else {
        sidebar.setAttribute('style', `transform: translateY(0px)`);
    }
    scrollNow = windowScroll;
};

function toogleSidebar(media) {
    if (media.matches) {
        window.addEventListener('load', sidebarPosition);
        window.addEventListener('scroll', sidebarPosition);
    } else {
        sidebar.removeAttribute('style');
        window.removeEventListener('load', sidebarPosition);
        window.removeEventListener('scroll', sidebarPosition);
    }
};

if (sidebar) {
    toogleSidebar(media)
    media.addListener(toogleSidebar)
};
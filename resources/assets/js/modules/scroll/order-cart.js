import PerfectScrollbar from 'perfect-scrollbar';

let media = window.matchMedia('(min-width: 991.98px)'),
    initPs = null;

const order = document.querySelector('.js-cart-order');

function initScroll(order) {
    initPs = new PerfectScrollbar(order, {
        suppressScrollX: true,
        swipeEasing: true,
        Wheelpropagation: false
    });

    order.addEventListener('ps-scroll-y', toogleShadow);

    window.addEventListener('resize', function () {
        if(initPs) {
            initPs.update();
        }
    });

    function toogleShadow() {
        if(order.classList.contains('ps--active-y')) {
            order.parentNode.querySelector('.c-cart-subtitle').classList.add('has-shadow');
        } 
        else {
            order.parentNode.querySelector('.c-cart-subtitle').classList.remove('has-shadow');
        }
    };

    toogleShadow();
};

function checkMedia(media) {
    if (media.matches) {        
        initScroll(order)
    } else {
        if (initPs) {
            initPs.destroy()
        }
        initPs = null;
    }
}

if (order) {
    checkMedia(media)
    media.addListener(checkMedia)
};
import PerfectScrollbar from 'perfect-scrollbar';
import './sidebar';
import './category';
import './order-cart';

const scrollElVertical = document.querySelectorAll('.js-scroll-y');
if(scrollElVertical) {
    var initElVertical = [];
    for (let index = 0; index < scrollElVertical.length; index++) {
        initElVertical.push(scrollElVertical[index]);
        initElVertical[index] = new PerfectScrollbar(initElVertical[index], {
            suppressScrollX: true
        });
    }
};

function updateScroll() {
    for(let i = 0; i < scrollElVertical.length; i++ ) {
        initElVertical[i].update();
    };
};

window.addEventListener('resize', function () {
    updateScroll()
});
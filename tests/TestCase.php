<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function createProduct($attrs = [])
    {
        if (empty($attrs['category_id'])) {
            $attrs['category_id'] = factory('App\Category')->create([
                'is_active' => 1,
            ])->id;
        }

        return factory('App\Product')->create($attrs);
    }
}

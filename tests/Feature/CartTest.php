<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Tag;
use App\Setting;
use App\Category;
use AlbertCht\InvisibleReCaptcha\Facades\InvisibleReCaptcha;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewOrderNotification;
use Illuminate\Support\Arr;

class CartTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNewEmptyCart()
    {
        $this->get('/api/cart')->assertStatus(200)->assertJson([
            'data' => [
                'products' => [],
            ],
        ]);
    }

    public function testCartAppendProductSuccess()
    {
        $product1 = $this->createProduct(['is_active' => 1]);

        $product2 = $this->createProduct(['is_active' => 1]);

        $dataSets = [
            [
                'data' => ['id' => $product1->id, 'amount' => 1],
                'result' => ['data' => ['products' => [
                    ['id' => $product1->id, 'amount' => 1],
                ]]],
            ],
            [
                'data' => ['id' => $product2->id, 'amount' => 1],
                'result' => ['data' => ['products' => [
                    ['id' => $product1->id, 'amount' => 1],
                    ['id' => $product2->id, 'amount' => 1],
                ]]],
            ],
            [
                'data' => ['id' => $product1->id, 'amount' => 0],
                'result' => ['data' => ['products' => [
                    ['id' => $product2->id, 'amount' => 1],
                ]]],
            ],
            [
                'data' => ['id' => $product2->id, 'amount' => 2],
                'result' => ['data' => ['products' => [
                    ['id' => $product2->id, 'amount' => 3],
                ]]],
            ],
        ];

        foreach ($dataSets as $dataSet) {
            $this->post('/api/cart', $dataSet['data'], ['Accept' => 'application/json'])
                ->assertStatus(200)
                ->assertJsonMissingValidationErrors()
                ->assertJson($dataSet['result']);
        }
    }

    public function testCartAppendProductInvalid()
    {
        $product = $this->createProduct(['is_active' => 1]);

        $dataSets = [
            [
                'data' => ['id' => $product->id, 'amount' => -1],
            ],
            [
                'data' => ['id' => $product->id, 'amount' => 1000],
            ],
            [
                'data' => ['id' => $product->id, 'amount' => 5.5],
            ],
        ];

        foreach ($dataSets as $dataSet) {
            $this->post('/api/cart', $dataSet['data'], ['Accept' => 'application/json'])
                ->assertStatus(422)
                ->assertJsonValidationErrors('amount')
            ;
        }
    }

    public function testCartUpdateAmountSuccess()
    {
        $product = $this->createProduct(['is_active' => 1]);

        $otherProduct = $this->createProduct(['is_active' => 1]);

        $this->post('/api/cart', [
            'id' => $product->id,
            'amount' => 500,
        ], ['Accept' => 'application/json']);

        $dataSets = [
            [
                'data' => ['id' => $product->id, 'amount' => 1],
                'result' => ['data' => ['products' => [
                    ['id' => $product->id, 'amount' => 1],
                ]]],
            ],
            [
                'data' => ['id' => $product->id, 'amount' => 0],
                'result' => ['data' => ['products' => [
                    //
                ]]],
            ],
            [
                'data' => ['id' => $product->id, 'amount' => 999],
                'result' => ['data' => ['products' => [
                    ['id' => $product->id, 'amount' => 999],
                ]]],
            ],
            [
                'data' => ['id' => $otherProduct->id, 'amount' => 1],
                'result' => ['data' => ['products' => [
                    ['id' => $product->id, 'amount' => 999],
                    ['id' => $otherProduct->id, 'amount' => 1],
                ]]],
            ],
        ];

        foreach ($dataSets as $dataSet) {
            $this->post('/api/cart/set_amount', $dataSet['data'], ['Accept' => 'application/json'])
                ->assertStatus(200)
                ->assertJsonMissingValidationErrors()
                ->assertJson($dataSet['result']);
        }
    }

    public function testCartSubmitSuccess()
    {
        $product = $this->createProduct(['is_active' => 1]);

        InvisibleReCaptcha::shouldReceive('verifyResponse')
            ->andReturn(true);

        Notification::fake();

        $cartData = $this->post('/api/cart', [
            'id' => $product->id,
            'amount' => 1
        ], ['Accept' => 'application/json'])->getData(true)['data'];

        $cart = \App\Cart::where('id', $cartData['id'])->first();

        $this->post('/api/cart/submit', [
            'g-recaptcha-response' => 'test',
            'payment' => 1,
            'delivery_time' => [
                'type' => 1,
            ],
            'address' => [
                'street' => 'test street',
            ],
            'name' => 'test name',
            'phone' => '+380991886699',
            'email' => 'test@example.com',
            'comment' => 'test comment',
        ], ['Accept' => 'application/json'])
            ->assertJsonMissingValidationErrors()
            ->assertStatus(201)
        ;

        $this->assertDatabaseHas('orders', [
            'cart_id' => $cart->id,
            'status' => 'new',
        ]);

        $administratorEmail = Arr::get(
            Setting::where('title', 'notification_email')->first(),
            'value'
        );

        Notification::assertSentTo(
            new AnonymousNotifiable,
            NewOrderNotification::class,
            function ($notification, $channels, $notifiable) use ($administratorEmail) {
                return $notifiable->routes['mail'] === $administratorEmail;
            }
        );

        Notification::assertSentTo(
            new AnonymousNotifiable,
            NewOrderNotification::class,
            function ($notification, $channels, $notifiable) {
                return $notifiable->routes['mail'] === 'test@example.com';
            }
        );
    }

    public function testCartSubmitSecurity()
    {
        $product = $this->createProduct(['is_active' => 1]);

        InvisibleReCaptcha::shouldReceive('verifyResponse')
            ->andReturn(true);

        Notification::fake();

        $cartData = $this->post('/api/cart', [
            'id' => $product->id,
            'amount' => 1
        ], ['Accept' => 'application/json'])->getData(true)['data'];

        $cart = \App\Cart::where('id', $cartData['id'])->first();

        $this->post('/api/cart/submit', [
            'g-recaptcha-response' => 'test',
            'payment' => 1,
            'delivery_time' => [
                'type' => 1,
            ],
            'address' => [
                'street' => 'test street',
            ],
            'name' => 'test name',
            'phone' => '+380991886699',
            'email' => 'test@example.com',
            'comment' => 'test comment',

            // fake data
            'cart_id' => 999,
            'status' => 'fake status',
            'total_price' => 0,
        ], ['Accept' => 'application/json'])
            ->assertJsonMissingValidationErrors()
            ->assertStatus(201)
        ;

        $this->assertDatabaseHas('orders', [
            'cart_id' => $cart->id,
            'status' => 'new',
            'total_price' => $cart->getTotalPrice(),
        ]);
    }

    public function testEmptyCartSubmit()
    {
        InvisibleReCaptcha::shouldReceive('verifyResponse')
            ->andReturn(true);

        Notification::fake();

        $this->post('/api/cart/submit', [
            'g-recaptcha-response' => 'test',
            'payment' => 1,
            'delivery_time' => [
                'type' => 1,
            ],
            'address' => [
                'street' => 'test street',
            ],
            'name' => 'test name',
            'phone' => '+380991886699',
            'email' => 'test@example.com',
            'comment' => null,
        ], ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJsonValidationErrors('cart')
        ;
    }

    public function testCartSubmitInvalid()
    {
        $product = $this->createProduct(['is_active' => 1]);

        InvisibleReCaptcha::shouldReceive('verifyResponse')
            ->andReturn(true);

        Notification::fake();

        $this->post('/api/cart', [
            'id' => $product->id,
            'amount' => 1
        ], ['Accept' => 'application/json']);

        $this->post('/api/cart/submit', [
            'g-recaptcha-response' => 'test',
            'payment' => null,
            'delivery_time' => null,
            'address' => null,
            'name' => null,
            'phone' => null,
            'email' => null,
            'comment' => null,
        ], ['Accept' => 'application/json'])
            ->assertJsonValidationErrors([
                'payment',
                'delivery_time.type',
                'address.street',
                'phone',
            ])
            ->assertStatus(422)
        ;

        Notification::assertNothingSent();
    }
}

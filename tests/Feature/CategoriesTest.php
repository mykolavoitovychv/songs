<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Tag;
use App\Category;

class CategoriesTest extends TestCase
{
    /**
     * @return void
     */
    public function testExistingCategories()
    {
        $categories = Category::active()->get();

        foreach ($categories as $category) {
            $this->get('/delivery/' . $category->slug)->assertStatus(200);
        }
    }

    /**
     * @return void
     */
    public function testNotExistingCategories()
    {
        $this->get('/delivery/not-exists')
            ->assertStatus(404);
    }
}

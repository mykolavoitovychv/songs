<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Tag;
use App\Category;

class OrderTest extends TestCase
{
    /**
     * @return void
     */
    public function testOrderPage()
    {
        $this->get('/order')
            ->assertStatus(200);
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('cart', 'Api\CartController@show')->name('cart.show');
Route::post('cart', 'Api\CartController@appendProduct')->name('cart.append-product');
Route::post('cart/set_amount', 'Api\CartController@setAmount')->name('cart.set_amount');
Route::post('cart/submit', 'Api\CartController@submit')->name('cart.submit');

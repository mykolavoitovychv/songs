<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('init:lang-files {lang}', function ($lang) {

    $src = base_path('vendor/caouecs/laravel-lang/src/' . $lang);
    $dest = resource_path('lang/' . $lang);

    $filesystem = new \Symfony\Component\Filesystem\Filesystem;
    $filesystem->mirror($src, $dest);

})->describe('Initialize lang files to resources/lang');

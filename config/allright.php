<?php

return [

    'checkers' => [

        [
            'checker' => Queens\LaravelAllright\Checkers\PageStatusChecker::class,
            'options' => ['url' => '/', 'status' => 302],
        ],

        [
            'checker' => Queens\LaravelAllright\Checkers\PageStatusChecker::class,
            'options' => ['url' => '/page-404', 'status' => 404],
        ],

        [
            'checker' => Queens\LaravelAllright\Checkers\PageStatusChecker::class,
            'options' => ['url' => '/delivery/popular', 'status' => 200],
        ],

        [
            'checker' => Queens\LaravelAllright\Checkers\RobotsTxtChecker::class,
            'options' => [],
        ],

        [
            'checker' => Queens\LaravelAllright\Checkers\IconChecker::class,
            'options' => ['url' => '/delivery/popular'],
        ],

        [
            'checker' => Queens\LaravelAllright\Checkers\MetaTagsChecker::class,
            'options' => ['url' => '/delivery/popular'],
        ],
    ],

];

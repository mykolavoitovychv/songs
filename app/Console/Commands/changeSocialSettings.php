<?php

namespace App\Console\Commands;

use App\Setting;
use Illuminate\Console\Command;

class changeSocialSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:change-social';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Setting::whereIn('title', ['instagram', 'facebook'])->delete();

        Setting::updateOrCreate([
            'title' => 'restaurant_facebook',
        ], [
            'value' => 'https://www.facebook.com/suligulicom/'
        ]);

        Setting::updateOrCreate([
            'title' => 'restaurant_instagram',
        ], [
            'value' => 'https://instagram.com/suliguli.rest'
        ]);

        Setting::updateOrCreate([
            'title' => 'bakery_facebook',
        ], [
            'value' => 'https://www.facebook.com/suliguli.pekarnya/'
        ]);

        Setting::updateOrCreate([
            'title' => 'bakery_instagram',
        ], [
            'value' => 'https://instagram.com/suliguli.pekarnia'
        ]);
    }
}

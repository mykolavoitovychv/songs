<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;

if (! function_exists('custom_mix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @return \Illuminate\Support\HtmlString|string
     *
     * @throws \Exception
     */
    function custom_mix($path, $manifestDirectory = '')
    {
        static $manifests = [];

        if (! Str::startsWith($path, '/')) {
            $path = "/{$path}";
        }

        if ($manifestDirectory && ! Str::startsWith($manifestDirectory, '/')) {
            $manifestDirectory = "/{$manifestDirectory}";
        }

        $manifestPath = public_path($manifestDirectory.'/manifest.json');

        if (! isset($manifests[$manifestPath])) {
            if (! file_exists($manifestPath)) {
                throw new Exception('The Mix manifest does not exist.');
            }

            $manifests[$manifestPath] = json_decode(file_get_contents($manifestPath), true);
        }

        $manifest = $manifests[$manifestPath];

        if (! isset($manifest[ltrim($path, '/')])) {
            $exception = new Exception("Unable to locate Mix file: {$path}.");

            if (! app('config')->get('app.debug')) {
                report($exception);

                return $path;
            } else {
                throw $exception;
            }
        }

        return new HtmlString($manifest[ltrim($path, '/')]);
    }
}



if (! function_exists('static_asset')) {
    /**
     * Get the path to front static file
     *
     * @param string $path
     * @param null $mixPath
     * @param null $imgDirectory
     * @return string
     * @throws Exception
     */
    function static_asset($path, $mixPath = null, $imgDirectory = null)
    {
        if ($mixPath) {
            $path = custom_mix($path, $mixPath);
        }

        if ($imgDirectory) {
            if (! Str::startsWith($path, '/')) {
                $path = "/{$path}";
            }
            $path = $imgDirectory . $path;
        }

        if (! Str::startsWith($path, ['//', 'http://', 'https://'])) {
            $path = config('app.asset_url') . '/' . ltrim($path, '/');
        }

        return $path;
    }
}

if (! function_exists('getModelFirstMediaUrl')) {
    /**
     * Get the path to front static file
     *
     * @param string $path
     * @param null $mixPath
     * @param null $imgDirectory
     * @return string
     * @throws Exception
     */
    function getModelFirstMediaUrl(Model $model, $collectionName = 'default', string $conversionName = ''): string
    {
        try {
            return $model->getFirstMediaUrl($collectionName, $conversionName);
        } catch (\Exception $e) {
            return '';
        }
    }
}

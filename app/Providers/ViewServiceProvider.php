<?php

namespace App\Providers;

use App\Cart;
use App\Delivery;
use App\Establishment;
use App\Setting;
use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::addNamespace('front', resource_path('front/views'));
        View::addNamespace('control', resource_path('control/views'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

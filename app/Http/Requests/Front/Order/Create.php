<?php

namespace App\Http\Requests\Front\Order;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $deliveryTime = $this->get('delivery_time');
        $dateError = false;
        $timeError = false;

        if (data_get($deliveryTime, 'type') == 'then') {
            $date = Carbon::parse(data_get($deliveryTime, 'date').' '.data_get($deliveryTime, 'time'));
            $now = Carbon::now();
            if ($date < $now) {
                if ($date->diffInDays($now)) {
                    $dateError = true;
                } else {
                    $timeError = true;
                }
            }
        }

        $rules =  [
            'g-recaptcha-response' => 'required|captcha',
            'name' => 'required|string',
            'phone' => 'required|phone:AUTO',
            'email' => 'nullable|email',
            'payment' => 'required|integer',
            'comment' => 'nullable|string|max:2000',
            'address.street' => 'required|string',
            'address.house' => 'required|string',
            'address.entrance' => 'nullable|integer',
            'address.code' => 'nullable|integer',
            'address.floor' => 'nullable|integer|max:30',
            'address.flat' => 'nullable|integer',
            'delivery_time.type' => 'required',
            'delivery_time.date' => [
                'required_if:delivery_time.type,then',
                'nullable',
                'date_format:d.m.Y',
                function ($attribute, $value, $fail) use ($dateError) {
                    if ($dateError) {
                        $fail('Вкажіть актуальну дату');
                    }
                },
                'after_or_equal:'.date('d.m.Y'),
            ],
            'delivery_time.time' => [
                'required_if:delivery_time.type,then',
                function ($attribute, $value, $fail) use ($timeError) {
                    if ($timeError) {
                        $fail('Вкажіть актуальний час');
                    }
                },
            ]
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'  => 'Вкажіть своє ім’я, щоб ми знали як до вас звертатись :)',
            'phone.required' => 'Вкажіть свій телефон, щоб ми могли з вами зв\'язатися :)',
            'phone.phone'    => 'Вкажіть свій телефон правильно, щоб ми могли з вами зв\'язатися :)',
            'address.street.required' => 'Вкажіть свою вулицю, щоб наш кур’єр знав куди їхати :)',
            'address.house.required' => 'Уточніть номер будинку',
            'delivery_time.date.required_if' => 'Поле є обов\'язковим для заповнення.',
            'delivery_time.date.after_or_equal' => 'Поле :attribute має містити дату не раніше, або дорівнюватися :date.',
            'delivery_time.time.required_if' => 'Поле є обов\'язковим для заповнення.',
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Cart;
use App\Category;
use App\Delivery;
use App\Tag;

class DeliveryController extends Controller
{
    public function index()
    {
        $activeCategory = Tag::active()
            ->ordered()
            ->first();

        if (!$activeCategory) {
            $activeCategory = Category::active()
                ->ordered()
                ->first();
        }
        return redirect()->route('delivery.category', ['category' => $activeCategory->slug]);
    }

    public function category()
    {
        $mainHomeBanner = Banner::where('slug', 'main_home_banner')
            ->active()
            ->with('slides.media')
            ->first();

        $categories = Category::active()
            ->ordered()
            ->with('activeProducts', 'activeProducts.media')
            ->get();

        $tags = Tag::active()
            ->ordered()
            ->with('activeProducts', 'activeProducts.media')
            ->get();

        $menuItems = $tags->concat($categories);
//        $activeCategory = $menuItems->where('slug', $category)->first();
//        if (!$activeCategory) {
//            abort(404);
//        }
        $currentCart = Cart::current();

        return view('front::pages.delivery.index', [
            'currentCart' => $currentCart,
            'menuItems' => $menuItems,
            'mainHomeBanner' => $mainHomeBanner
        ]);
    }

    public function order()
    {
        $cart = Cart::current();
        $delivery = Delivery::firstOrFail();

        return view('front::pages.order.index', compact('cart', 'delivery'));
    }
}

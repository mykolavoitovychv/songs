<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use App\CartProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests\Front\Order\Create as CreateRequest;
use App\Http\Requests\Front\Cart\Append as AppendRequest;
use App\Http\Resources\Front\Cart as CartResource;
use App\Notifications\NewOrderNotification;
use App\Notifications\NewOrderUserNotification;
use App\Order;
use App\Product;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class CartController extends Controller
{
    public function submit(CreateRequest $request)
    {
        $cart = Cart::current();

        if ($cart->cartProducts->isEmpty()) { // if user trying to resend the request after cleared cart
            throw ValidationException::withMessages([
                'cart' => ['Кошик порожній']
            ]);
        }

        $data = [
            'cart_id' => $cart->id,
            'products_price' => $cart->getBasePrice(),
            'delivery_price' => $cart->getDeliveryPrice(),
            'total_price' => $cart->getTotalPrice(),
            'payment_type' => $request->get('payment'),
            'delivery_time' => $request->get('delivery_time'),
            'address' => $request->get('address'),
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'comment' => $request->get('comment', ''),
            'status' => 'new',
        ];

        $order = Order::create($data);

        Notification::route('mail', Arr::get(Setting::where('title', 'notification_email')->first(), 'value'))
            ->notify(new NewOrderNotification($order));

        if ($email = $order->email) {
            Notification::route('mail', $email)
                ->notify(new NewOrderUserNotification($order));
        }

        $cart->close();

        return $order;
    }

    public function show(Request $request)
    {
        return new CartResource(Cart::current());
    }

    public function appendProduct(AppendRequest $request)
    {
        $this->addProduct($request, true);

        return new CartResource(
            Cart::current()
        );
    }

    public function setAmount(AppendRequest $request)
    {
        $this->addProduct($request, false);

        return new CartResource(
            Cart::current()
        );
    }

    protected function addProduct(Request $request, bool $stackAmount)
    {
        $product = Product::where('id', $request->get('id'))->firstOrFail();

        $amount = max(0, (int) $request->get('amount', 1));

        if (!$product->is_active && $amount > 0) {
            throw ValidationException::withMessages([
                'inactiveProducts' => array($product->id)
            ]);
        }

        DB::transaction(function () use ($product, $amount, $stackAmount) {
            $cart = Cart::current(true);

            if ($amount <= 0) {
                CartProduct::where('cart_id', $cart->id)
                    ->where('product_id', $product->id)
                    ->delete();
            } else {
                $cartProduct = CartProduct::firstOrNew([
                    'cart_id' => $cart->id,
                    'product_id' => $product->id
                ], [
                    'amount' => 0
                ]);

                if ($stackAmount) {
                    $cartProduct->amount += $amount;
                } else {
                    $cartProduct->amount = $amount;
                }
                $cartProduct->save();
            }
        });
    }
}

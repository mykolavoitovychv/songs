<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function indexPage()
    {
        return view('front::pages.index');
    }
}

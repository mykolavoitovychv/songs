<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;

class Cart extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $products = Product::collection($this->resource->products);

        return [
            'id' => $this->id,
            'created_at' => $this->resource->created_at->toAtomString(),
            'products' => $products,
            'products_html' => view('front::components.modals.cart_items', ['products' => $products])->render(),
            'delivery_price' => (int)$this->resource->getDeliveryPrice(),
            'base_price' => (int)$this->resource->getBasePrice(),
            'total_price' => (int)$this->resource->getTotalPrice(),
        ];
    }
}

<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Media as MediaResource;
use Illuminate\Support\Arr;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attrs = $this->resource->getAttributes();

        return [
            'id' => $attrs['id'],
            'title' => $attrs['title'],
            'description' => $attrs['description'],
            'price' => round($attrs['price']),
            'weight' => $attrs['weight'],
            // 'created_at' => $attrs['created_at'],
            // 'updated_at' => $attrs['updated_at'],
            // 'id' => $this->resource->id,
            // 'title' => $this->resource->title,
            // 'description' => $this->resource->description,
            // 'price' => (int)$this->resource->price,
            // 'weight' => $this->resource->weight,
            // 'category_id' => $this->resource->category_id,
            // 'is_active' => $this->resource->is_active,
            // 'created_at' => $this->resource->created_at,
            // 'updated_at' => $this->resource->updated_at,
            'amount' => optional($this->resource->pivot)->amount ?? 0,
            'mainImageUrl' => $this->resource->getFirstMediaUrl('main', '320x320'),
            'mainImageUrlx2' => $this->resource->getFirstMediaUrl('main', '640x640'),
            'modalImage' => [
                '530x380' => $this->resource->getFirstMediaUrl('modal', '530x380'),
                '1060x760' => $this->resource->getFirstMediaUrl('modal', '1060x760'),
                '376x305' => $this->resource->getFirstMediaUrl('modal', '376x305'),
                '751x609' => $this->resource->getFirstMediaUrl('modal', '751x609'),
            ],
        ];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    protected $fillable = [
        'word_1',
        'word_2',
        'word_3',
        'word_4',
        'word_5'
    ];
}

<?php

namespace Ideil\LaravelInky\Engine;

use DOMDocument;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Illuminate\View\Compilers\CompilerInterface;
use Illuminate\View\Engines\CompilerEngine;
use Pelago\Emogrifier;
use Pinky;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

class Engine extends CompilerEngine
{
    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var array
     */
    protected $config;

    /**
     * Engine constructor.
     * @param CompilerInterface $compiler
     * @param Filesystem $files
     * @param array $config
     */
    public function __construct(CompilerInterface $compiler, Filesystem $files, array $config = [])
    {
        parent::__construct($compiler);

        $this->files = $files;
        $this->config = $config;
    }

    /**
     * @param $styles
     * @param string $separator
     * @return string
     */
    protected function getMediaQueries($styles, $separator = "\n\n"): string
    {
        $matches = [];

        \preg_match_all(
            '/@media[^{]+\{([\s\S]+?})\s*}/',
            $styles,
            $matches
        );

        $mediaQueries = $matches[0] ?? [];

        return \implode($separator, $mediaQueries);
    }

    /**
     * @return string
     */
    protected function getLayout(): string
    {
        return \file_get_contents(
            $this->config['template'] ?? dirname(__DIR__, 2) . '/resources/html/layout.html'
        );
    }

    /**
     * @param DOMDocument $dom
     * @return DOMDocument
     */
    protected function removeStyleSheetTags(DOMDocument $dom): DOMDocument
    {
        $stylesheets = $dom->getElementsByTagName('stylesheet');

        foreach ($stylesheets as $stylesheet) {
            $stylesheet->parentNode->removeChild($stylesheet);
        }

        return $dom;
    }

    /**
     * @param DOMDocument $dom
     * @return array
     */
    protected function getStyleSheets(DOMDocument $dom): array
    {
        $styles = [];
        $stylesheets = $dom->getElementsByTagName('stylesheet');

        foreach ($stylesheets as $stylesheet) {
            $styles[] = $stylesheet->getAttribute('src');
        }

        return $styles;
    }

    /**
     * @param string $html
     * @return DOMDocument
     * @throws \ErrorException
     */
    protected function getDOM($html)
    {
        $dom = new DOMDocument();

        try {
            $dom->loadHTML('<?xml encoding="UTF-8">' . $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        } catch (\ErrorException $e) {
            if ($e->getSeverity() !== 2) {
                throw $e;
            }
        }

        return $dom;
    }

    /**
     * @param string $__path
     * @param array $__data
     * @return string
     * @throws \ErrorException
     */
    protected function evaluatePath($__path, $__data)
    {
        $result = parent::evaluatePath($__path, $__data);

        $dom = $this->getDOM($result);

        $styles = $this->getStyleSheets($dom);

        $this->removeStyleSheetTags($dom);

        $stylesContent = collect($styles)
            ->map(function (string $style) {
                return $this->files->get($style);
            })
            ->implode("\n\n");

        $html = \str_replace(
            '<?xml encoding="UTF-8">',
            '',
            Pinky\transformDOM($dom)->saveHTML()
        );

        $html = str_replace(
            ['SUBJECT_CONTENT', 'BODY_CONTENT', 'MEDIA_QUERIES'],
            [
                Arr::get($__data, 'subject', ''),
                $html,
                $this->getMediaQueries($stylesContent)
            ],
            $this->getLayout()
        );

        return (new CssToInlineStyles)->convert($html, $stylesContent);
    }
}

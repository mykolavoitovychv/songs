<?php

namespace Ideil\LaravelInky\Providers;

use Ideil\LaravelInky\Engine\Engine;
use Illuminate\Support\ServiceProvider;

class LaravelInkyServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        $this->app['view']->addExtension('inky.php', 'inky');

        $config = \dirname(__DIR__, 2) . '/resources/config/laravel-inky.php';

        $this->publishes([
            $config => config_path('laravel-inky.php'),
        ], 'configs');

        $this->mergeConfigFrom($config, 'laravel-inky');
    }

    /**
     * @return void
     */
    public function register()
    {
        $this->app['view.engine.resolver']->register('inky', function () {
            return new Engine(
                $this->app['blade.compiler'],
                $this->app['files'],
                $this->app['config']->get('laravel-inky')
            );
        });
    }
}

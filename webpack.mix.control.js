let path = require('path')
let mix = require('laravel-mix')
let resourcesBasePath = './resources/front'
let staticBasePath = './static/front'
let ImageMin = require('imagemin-webpack-plugin').default
let Webp = require('imagemin-webp')
let MozJpeg = require('imagemin-mozjpeg')
let Copy = require('copy-webpack-plugin')
let {
    CleanWebpackPlugin
} = require('clean-webpack-plugin')
// let criticalCssPages = [
//     {
//         url: '/',
//         template: 'home'
//     }
// ]

// PublicPath
mix.setPublicPath(staticBasePath)

// Configuration
mix.webpackConfig({
    resolve: {
        alias: {
            '@root': __dirname,
            '@components': path.resolve(__dirname, resourcesBasePath + '/js/components'),
            '@partials': path.resolve(__dirname, resourcesBasePath + '/js/partials'),
            '@system': path.resolve(__dirname, resourcesBasePath + '/js/system'),
            '@pages': path.resolve(__dirname, resourcesBasePath + '/js/pages'),
            '@lang': path.resolve(__dirname, resourcesBasePath + '/js/system/translations/lang')
        }
    }
})
mix.options({
    processCssUrls: false
})

// Files
mix.js(resourcesBasePath + '/js/app.js', 'js')
    .sass(resourcesBasePath + '/sass/app.scss', 'css', {
        implementation: require('node-sass')
    })

// build env settings
if (mix.inProduction()) {
    mix.version()

    // Generate critical css after assets compilation
    mix.webpackConfig({
        plugins: [
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: ['img', 'fonts']
            }),
            // {
            //     apply (compiler) {
            //         compiler.hooks.afterEmit.tapPromise('GenerateCriticalCss', async (compilation, callback) => {
            //             console.log('Generating critical css...')

            //             var critical = require('critical')
            //             const fs = require('fs')

            //             const staticBasePath = (process.env.ASSET_URL.replace(/^(https?:)\/\/.+?\//, '/') + '/')
            //                 .replace(/\/{2,}/, '/')

            //             await Promise.all(criticalCssPages.map(function (criticalCssPage) {
            //                 return new Promise(async function (resolve, reject) {
            //                     const fullUrl = process.env.APP_URL + criticalCssPage.url

            //                     console.log('Critical css [' + criticalCssPage.template + ']: ' + fullUrl)

            //                     const result = await critical.generate({
            //                         src: fullUrl,
            //                         dimensions: [{
            //                             width: 320,
            //                             height: 568
            //                         }, {
            //                             width: 1920,
            //                             height: 1080
            //                         }],
            //                         minify: true,
            //                         ignore: {
            //                             atrule: ['@font-face']
            //                         },
            //                         rebase: (asset) => {
            //                             return '{{ asset(\'' + asset.absolutePath.slice(staticBasePath.length) + '\') }}'
            //                         }
            //                     })

            //                     const dir = resourcesBasePath + '/views/critical/'
            //                     const filePath = dir + criticalCssPage.template + '.blade.php'

            //                     fs.writeFile(filePath, '<style>' + result.css + '</style>', function (err) {
            //                         if (err) {
            //                             reject(err)
            //                         } else {
            //                             resolve()
            //                         }
            //                     })
            //                 })
            //             }))

            //             console.log('Generating critical css is done.')
            //         })
            //     }
            // },
            new Copy([{
                from: resourcesBasePath + '/fonts',
                to: 'fonts'
            }]),
            new Copy([{
                from: resourcesBasePath + '/img',
                to: 'img'
            }]),
            new ImageMin({
                disable: process.env.NODE_ENV !== 'production',
                pngquant: ({
                    quality: '75'
                }),
                svgo: {
                    plugins: [{
                            removeViewBox: false
                        },
                        {
                            removeTitle: false
                        }
                    ]
                },
                plugins: [
                    MozJpeg({
                        quality: '75'
                    })
                ]
            }),
            new Copy([{
                context: resourcesBasePath + '/img',
                from: '**/*.+(jpg|jpeg|png)',
                to: 'img/',
                transformPath(targetPath, absolutePath) {
                    return targetPath.replace(/\.(jpe?g|png)$/i, '.webp')
                }
            }]),
            new ImageMin({
                disable: process.env.NODE_ENV !== 'production',
                test: /\.(webp)$/i,
                plugins: [
                    Webp({
                        quality: 75
                    })
                ]
            })
        ]
    })
} else {
    mix.webpackConfig({
        plugins: [
            new Copy([{
                from: resourcesBasePath + '/fonts',
                to: 'fonts'
            }]),
            new Copy([{
                from: resourcesBasePath + '/img',
                to: 'img'
            }]),
            new Copy([{
                context: resourcesBasePath + '/img',
                from: '**/*.+(jpg|jpeg|png)',
                to: 'img/',
                transformPath(targetPath, absolutePath) {
                    return targetPath.replace(/\.(jpe?g|png)$/i, '.webp')
                }
            }])
        ]
    })

    mix.sourceMaps()
        .browserSync({
            proxy: process.env.APP_URL,
            server: {
                baseDir: staticBasePath,
                directory: true
            },
            files: [
                resourcesBasePath + '/views/**/*.php',
                staticBasePath + '/css/**/*',
                staticBasePath + '/js/**/*',
                staticBasePath + '/img/**/*',
                staticBasePath + '/html/**/*'
            ],
            notify: false,
            browser: 'google chrome',
            ghostMode: false
        })
}
